/*
* Copyright 2012 Ryan Ackley (ryanackley@gmail.com)
*
* This file is part of NPAPI Chrome File API
*
* NPAPI Chrome File API is free software: you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#import "FileWatcherTask.h"
#include "variant_list.h"

void fsevents_callback(ConstFSEventStreamRef streamRef,
                       void *userData,
                       size_t numEvents,
                       void *eventPaths,
                       const FSEventStreamEventFlags eventFlags[],
                       const FSEventStreamEventId eventIds[])
{
    FileWatcherTask *fw = (FileWatcherTask *)userData;
	size_t i;
	for(i=0; i<numEvents; i++){
        [fw postCallback:[(NSArray *)eventPaths objectAtIndex:i]];
	}
    
}

@implementation FileWatcherTask

-(id)initWithCallback:(FB::JSObjectPtr)obj
{
    self = [super init];
    callback = obj;
    fm = [NSFileManager defaultManager];
    pathModificationDates = [[NSMutableDictionary alloc] initWithCapacity:2];
    return self;
    
}

-(void)listenForFileEvents:(NSString*)path
{
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    NSArray *pathsToWatch = [NSArray arrayWithObject:path];
    appStartedTimestamp = [NSDate date];
    
    void *appPointer = (void *)self;
    FSEventStreamContext context = {0, appPointer, NULL, NULL, NULL};
    NSTimeInterval latency = 1.0;
    stream = FSEventStreamCreate(NULL,
                                 &fsevents_callback,
                                 &context,
                                 (CFArrayRef) pathsToWatch,
                                 kFSEventStreamEventIdSinceNow, 
                                 (CFAbsoluteTime) latency,
                                 kFSEventStreamCreateFlagUseCFTypes
                                 );
    runLoop = CFRunLoopGetCurrent();
    FSEventStreamScheduleWithRunLoop(stream, runLoop, kCFRunLoopDefaultMode);
	FSEventStreamStart(stream);
    CFRunLoopRun();
    [pool release];
}

-(void)postCallback:(NSString*)path
{
    
    NSArray *contents = [fm directoryContentsAtPath:path];
    NSString* fullPath = nil;
    
    for(NSString* node in contents) 
    {
        fullPath = [NSString stringWithFormat:@"%@%@",path,node];
        
        NSDictionary *fileAttributes = [fm attributesOfItemAtPath:fullPath error:NULL];
        NSDate *fileModDate =
        [fileAttributes objectForKey:NSFileModificationDate];
        if([fileModDate compare:[self lastModificationDateForPath:path]] == NSOrderedDescending) 
        {
            std::string stdPath = [fullPath UTF8String];
            callback->Invoke("",FB::variant_list_of(stdPath));
        }
    }
    [self updateLastModificationDateForPath:path];
}

- (void)updateLastModificationDateForPath: (NSString *)path
{
	[pathModificationDates setObject:[NSDate date] forKey:path];
}

- (void)stopWatching
{
    CFRunLoopStop(runLoop);
}

- (NSDate *)lastModificationDateForPath: (NSString *)path
{
	if(nil != [pathModificationDates valueForKey:path]) {
		return [pathModificationDates valueForKey:path];
	}
	else{
		return appStartedTimestamp;
	}
}

@end
