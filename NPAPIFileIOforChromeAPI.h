/*
* Copyright 2012 Ryan Ackley (ryanackley@gmail.com)
*
* This file is part of NPAPI Chrome File API
*
* NPAPI Chrome File API is free software: you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include <string>
#include <sstream>
#include <boost/weak_ptr.hpp>
#include "JSAPIAuto.h"
#include "BrowserHost.h"
#include "NPAPIFileIOforChrome.h"

#ifndef H_NPAPIFileIOforChromeAPI
#define H_NPAPIFileIOforChromeAPI

class NPAPIFileIOforChromeAPI : public FB::JSAPIAuto
{
public:
    ////////////////////////////////////////////////////////////////////////////
    /// @fn NPAPIFileIOforChromeAPI::NPAPIFileIOforChromeAPI(const NPAPIFileIOforChromePtr& plugin, const FB::BrowserHostPtr host)
    ///
    /// @brief  Constructor for your JSAPI object.
    ///         You should register your methods, properties, and events
    ///         that should be accessible to Javascript from here.
    ///
    /// @see FB::JSAPIAuto::registerMethod
    /// @see FB::JSAPIAuto::registerProperty
    /// @see FB::JSAPIAuto::registerEvent
    ////////////////////////////////////////////////////////////////////////////
    NPAPIFileIOforChromeAPI(const NPAPIFileIOforChromePtr& plugin, const FB::BrowserHostPtr& host) :
        m_plugin(plugin), m_host(host)
    {
        registerMethod("echo",      make_method(this, &NPAPIFileIOforChromeAPI::echo));
        registerMethod("testEvent", make_method(this, &NPAPIFileIOforChromeAPI::testEvent));
        //registerMethod("saveToFileUrl",      make_method(this, &NPAPIFileIOforChromeAPI::saveToFileUrl));
        registerMethod("launchFolderSelect",     make_method(this, &NPAPIFileIOforChromeAPI::launchFolderSelect));
        registerMethod("launchFileSelect",     make_method(this, &NPAPIFileIOforChromeAPI::launchFileSelect));
        registerMethod("watchDirectory",     make_method(this, &NPAPIFileIOforChromeAPI::watchDirectory));
        registerMethod("stopWatching",     make_method(this, &NPAPIFileIOforChromeAPI::stopWatching));
        registerMethod("createDirectory",      make_method(this, &NPAPIFileIOforChromeAPI::createDirectory));
        registerMethod("saveBlobToFile",      make_method(this, &NPAPIFileIOforChromeAPI::saveBlobToFile));
        registerMethod("getDirEntries",      make_method(this, &NPAPIFileIOforChromeAPI::getDirEntries));
        registerMethod("contentsAtPath",      make_method(this, &NPAPIFileIOforChromeAPI::contentsAtPath));
        registerMethod("getFileSize",      make_method(this, &NPAPIFileIOforChromeAPI::getFileSize));
        registerMethod("isDirectory",      make_method(this, &NPAPIFileIOforChromeAPI::isDirectory));
        registerMethod("fileExists",      make_method(this, &NPAPIFileIOforChromeAPI::fileExists));
        registerMethod("removeRecursively",      make_method(this, &NPAPIFileIOforChromeAPI::removeRecursively));
        registerMethod("getChromeDataDir",      make_method(this, &NPAPIFileIOforChromeAPI::getChromeDataDir));
        
        // Read-write property
        registerProperty("testString",
                         make_property(this,
                                       &NPAPIFileIOforChromeAPI::get_testString,
                                       &NPAPIFileIOforChromeAPI::set_testString));
        
        // Read-only property
        registerProperty("version",
                         make_property(this,
                                       &NPAPIFileIOforChromeAPI::get_version));
    }

    ///////////////////////////////////////////////////////////////////////////////
    /// @fn NPAPIFileIOforChromeAPI::~NPAPIFileIOforChromeAPI()
    ///
    /// @brief  Destructor.  Remember that this object will not be released until
    ///         the browser is done with it; this will almost definitely be after
    ///         the plugin is released.
    ///////////////////////////////////////////////////////////////////////////////
    virtual ~NPAPIFileIOforChromeAPI() {};

    NPAPIFileIOforChromePtr getPlugin();

    // Read/Write property ${PROPERTY.ident}
    std::string get_testString();
    void set_testString(const std::string& val);

    // Read-only property ${PROPERTY.ident}
    std::string get_version();

    // Method echo
    FB::variant echo(const FB::variant& msg);
    
    // Event helpers
    FB_JSAPI_EVENT(test, 0, ());
    FB_JSAPI_EVENT(echo, 2, (const FB::variant&, const int));

    // Method test-event
    void testEvent();
    
    //void saveToFileUrl(std::string url, std::string content);
    bool createDirectory(std::string path);
    bool saveBlobToFile(std::string path, FB::JSObjectPtr dataArray);
    FB::VariantList getDirEntries(std::string path);
    FB::JSAPIPtr contentsAtPath(std::string path);
    int getFileSize(std::string path);
    bool isDirectory(std::string path);
    bool fileExists(std::string path);
    bool removeRecursively(std::string path);
    void launchFolderSelect(FB::JSObjectPtr callback);
    void launchFileSelect(FB::JSObjectPtr callback); 
    void watchDirectory(std::string key, std::string path, FB::JSObjectPtr callback);
    void stopWatching(std::string key);
    std::string getChromeDataDir(std::string version);

private:
    
    void fileSelectCallback(const std::string &path, FB::JSObjectPtr callback);
    
    NPAPIFileIOforChromeWeakPtr m_plugin;
    FB::BrowserHostPtr m_host;

    std::string m_testString;
};

#endif // H_NPAPIFileIOforChromeAPI

