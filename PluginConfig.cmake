#/**********************************************************\ 
#
# Auto-Generated Plugin Configuration file
# for NPAPI File IO for Chrome
#
#\**********************************************************/

set(PLUGIN_NAME "NPAPIFileIOforChrome")
set(PLUGIN_PREFIX "NFIFC")
set(COMPANY_NAME "BenryanSoftwareInc")

# ActiveX constants:
set(FBTYPELIB_NAME NPAPIFileIOforChromeLib)
set(FBTYPELIB_DESC "NPAPIFileIOforChrome 1.0 Type Library")
set(IFBControl_DESC "NPAPIFileIOforChrome Control Interface")
set(FBControl_DESC "NPAPIFileIOforChrome Control Class")
set(IFBComJavascriptObject_DESC "NPAPIFileIOforChrome IComJavascriptObject Interface")
set(FBComJavascriptObject_DESC "NPAPIFileIOforChrome ComJavascriptObject Class")
set(IFBComEventSource_DESC "NPAPIFileIOforChrome IFBComEventSource Interface")
set(AXVERSION_NUM "1")

# NOTE: THESE GUIDS *MUST* BE UNIQUE TO YOUR PLUGIN/ACTIVEX CONTROL!  YES, ALL OF THEM!
set(FBTYPELIB_GUID 4e46b83d-4055-5fd3-916c-c0a142b50268)
set(IFBControl_GUID b46273de-1ce1-57c7-b818-86e524e359af)
set(FBControl_GUID 2de9b1c0-4533-592c-b41e-cb343a22541b)
set(IFBComJavascriptObject_GUID cd48eb78-e554-58e9-ac96-b4398ceb2d54)
set(FBComJavascriptObject_GUID 0ba6037b-28b9-5561-b775-78a7b9a60b16)
set(IFBComEventSource_GUID 0453fead-e87e-5cc5-828a-0efe94ad7596)

# these are the pieces that are relevant to using it from Javascript
set(ACTIVEX_PROGID "BenryanSoftwareInc.NPAPIFileIOforChrome")
set(MOZILLA_PLUGINID "benryan.com/NPAPIFileIOforChrome")

# strings
set(FBSTRING_CompanyName "Benryan Software Inc.")
set(FBSTRING_FileDescription "Adds file operations to a javascript api for Chrome")
set(FBSTRING_PLUGIN_VERSION "1.0.0.0")
set(FBSTRING_LegalCopyright "Copyright 2012 Benryan Software Inc.")
set(FBSTRING_PluginFileName "np${PLUGIN_NAME}.dll")
set(FBSTRING_ProductName "NPAPI File IO for Chrome")
set(FBSTRING_FileExtents "")
set(FBSTRING_PluginName "NPAPI File IO for Chrome")
set(FBSTRING_MIMEType "application/x-npapifileioforchrome")

# Uncomment this next line if you're not planning on your plugin doing
# any drawing:

set (FB_GUI_DISABLED 0)

# Mac plugin settings. If your plugin does not draw, set these all to 0
set(FBMAC_USE_QUICKDRAW 0)
set(FBMAC_USE_CARBON 0)
set(FBMAC_USE_COCOA 0)
set(FBMAC_USE_COREGRAPHICS 0)
set(FBMAC_USE_COREANIMATION 0)
set(FBMAC_USE_INVALIDATINGCOREANIMATION 0)

# If you want to register per-machine on Windows, uncomment this line
#set (FB_ATLREG_MACHINEWIDE 1)

set (BOOST_FILESYSTEM_V3 1)
add_boost_library(filesystem)
