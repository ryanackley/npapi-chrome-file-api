#About#

This is a NPAPI browser plugin that exposes an API for reading and writing files to the native file system. In addition, it also allows for watching directories for changes to files and also launching a folder select dialog box. It should compile and work on all 3 major platforms (Windows, OS X, Linux).

It was written for Chrome but you could probably get it working for other browsers with minimal effort.

#Building#

This plugin was created using [Firebreath](http://firebreath.org), a cross-platform, cross-browser API for writing NPAPI plugins. This means you will need to use Firebreath to build it. The following build instructions are based on the [FireBreath docs](http://www.firebreath.org/display/documentation/FireBreath+Home). I've just tried to condense them. If you get stuck or lost, go check out the [FireBreath build instructions](http://www.firebreath.org/display/documentation/Building+FireBreath+Plugins).

Pre-requisites
-----------------

* All platforms require [CMake version 2.8](http://www.cmake.org/cmake/resources/software.html)
* On Mac OS X, you will need Xcode. It's a free download and can be installed via the App store.
* On Windows, Visual Studio 2005 or later. Note, it appears to be possible to build using the [free version of Visual Studio](http://www.firebreath.org/display/documentation/Building+with+Visual+Studio+Express) although I haven't tried this since I have the full version. 
* On Linux, gcc, make, libgtk2.0-dev, libxmu-dev, and git.

Build Steps
----------------

1. [Download firebreath](http://www.firebreath.org/display/documentation/Download) and unzip it into it's own directory. Firebreath 1.6 is known to work.
2. Clone this project into it's own directory (i.e. 'git clone https://ryanackley@bitbucket.org/ryanackley/npapi-chrome-file-api.git') 
3. Create an output directory for the firebreath build scripts. Mine is named 'fbbuild'. You can name yours whatever you want.
4. Run the Firebreath prep script for your platform. Assuming you followed the previous steps and now have 3 directories under your current diectory: 'firebreath', 'npapi-chrome-file-api', 'fbbuild', the command line will look like this. 

	* Mac: 'firebreath/prepmac.sh npapi-chrome-file-api fbbuild'
	* Linux: 'firebreath/prepmake.sh npapi-chrome-file-api fbbuild'
	* Windows: 'firebreath/prep2008.cmd npapi-chrome-file-api fbbuild'

	These scripts prep the build for your favorite build tool. You will still need to use a compiler or IDE to compile and link the binary NPAPI library. For example, on Mac OS X, the prepmac.sh script generates an XCode project (therefore you need to have XCode on Mac to build this project). For Linux and Windows builds there are several options. In the example above, I prep the Linux build to use Make and the Windows build to use Visual Studio 2008. For a full explanation of the process and available options refer to the [Firebreath build documentation](http://www.firebreath.org/display/documentation/Building+FireBreath+Plugins).

5. After you've successfully run the prep script for your platform and desired build tool. The required build files should now be present in the fbbuild directory. The next step depends on the platform and build tool. If you're using an IDE, the project files should be in the 'fbbuild' directory. If you're using Make the makefile will be there instead.
6. You should now be able to build and compile the NPAPI library. 
