/*
* Copyright 2012 Ryan Ackley (ryanackley@gmail.com)
*
* This file is part of NPAPI Chrome File API
*
* NPAPI Chrome File API is free software: you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#ifndef FILEWATCHERTASKX11_H_
#define FILEWATCHERTASKX11_H_
#define BOOST_FILESYSTEM_VERSION 3

#include "JSObject.h"
#include <boost/filesystem.hpp>
#include <boost/thread.hpp>

class FileWatcherTaskX11 {
public:
	FileWatcherTaskX11(){stop = 0;};
	~FileWatcherTaskX11(){};
	void Watch(boost::filesystem::path path, FB::JSObjectPtr callback);
	void StopWatching();

private:
	void ListenForFileEvents(boost::filesystem::path path, FB::JSObjectPtr callback);
	void AddWatchToDir(int fd, boost::filesystem::path path, std::map<int, boost::filesystem::path*> &watchDescriptors);
	bool _ShouldStop();
	int stop;
	boost::thread thread;
};

#endif /* FILEWATCHERTASKX11_H_ */
